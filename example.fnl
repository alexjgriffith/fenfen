;; Start of example.fnl
(local example {})

(fn example.run [a]
  (print (.. "Running " a)))

example
