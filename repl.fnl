(local js (require "js"))
(local fennel (require "lib.fennel"))
(local log (fn [...] (: js.global.console :log ...)))

;; (local env (setmetatable {:js js :fennel fennel :log log}
;;                          {:__index _G}))

;; (set env._ENV env)

(var last-input nil)
(var last-value nil)

(local friend (require :lib.fennelfriend))

(partial fennel.repl {:moduleName "fennel/fennel"
                      :readChunk (fn []
                                   (let [input (coroutine.yield)]
                                     (set last-input input)
                                     (print (.. "> " input))
                                     (.. input "\n")))
                      :onValues (fn [xs]
                                  (print (table.concat xs "\t"))
                                  (set last-value (. xs 1)))
                      ;; TODO: make errors red
                      ;; TODO: log errors for analysis?
                      :onError (fn [_ msg] (printError msg))
                      :pp (require :lib.fennelview)
                      :assert-compile friend.assert-compile
                      :parse-error friend.parse-error
                      :env nil})
